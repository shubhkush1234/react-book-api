How to run this application?

1. Start the JSON Server (at port:3000): "npm run shubhamServer" 

    It will start json server.
    Server URL: "http://localhost:3000/books/"

2. Running the development server (at Port:3010): "npm start". 
    URL: "http://localhost:3010/"

I have used json server for fake REST API. Used reactstrap and bootstrap as css library.
reactstrap.github.io

API operations implemented:
1. GET
2. POST
3. PUT
4. DELETE
