import axios from 'axios';
//import api from './api';

let axiosObj = {};
const baseUrl = "http://localhost:3000";

axiosObj.fetchTableData = () => {
    return axios({
        method: 'get',
        headers: {
            'Access-control-Allow-Origin': '*',
            'Access-control-Allow-Headers': '*'
        },
        url: baseUrl + "/books",
        data: null
    })
};
// Not working
axiosObj.updateTableData = ({title, rating}) => {
    return axios({
        method: 'post',
        headers: {
            'Access-control-Allow-Origin': '*',
            'Access-control-Allow-Headers': '*'
        },
        url: "http://localhost:3000/books"+ this.state.editBookData.id,
        data: {title, rating}
    })

}

export default axiosObj;