import React, { Component } from 'react';
import { Table, Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Input, Label, Container } from 'reactstrap';
import Axios from 'axios';
//import api  from './services/api';
import axiosObj from './services/tableServices';

class App extends Component {

  state = {
    books: [],
    newBookModal: false,
    toggleEditBookModel: false,
    editBookModal: false,
    editBookData: {
      id: '',
      title: '',
      rating: ''
    },
    newBookData: {
      title: '',
      rating: ''
    },
    watchedTill: 34.59
  }

  _refreshTable() {
    axiosObj.fetchTableData().then((response => {
      this.setState({
        books: response.data
      })
    })).catch(error => console.log(error, "error caused in GET API"));
  }

  addBook() {
    Axios.post("http://localhost:3000/books", this.state.newBookData).then((postResponse) => {
      console.log(postResponse.data, "postResponse");
      let { books } = this.state;
      books.push(postResponse.data);
      this.setState({
        books,
        newBookData: {
          title: '',
          rating: ''
        },
        newBookModal: false
      })
    }).catch((error) => {
      console.log(error, "error caused on POST API");
    }
    )
  }

  componentDidMount() {
    this._refreshTable();
  }

  toggleNewBookModel() {
    this.setState({
      newBookModal: !this.state.newBookModal //will give opposite of what's there presently
    })
  };

  toggleEditBookModel() {
    this.setState({
      editBookModal: !this.state.editBookModal //will give opposite of what's there presently
    })
  };

  editBookDataModalHandler(id, title, rating) {
    console.log(title, "title");
    this.setState({
      editBookData: { id, title, rating },
      editBookModal: !this.state.editBookModal
    });
  };

  updateBookData() {
    const { title, rating } = this.state.editBookData;
    Axios.put("http://localhost:3000/books/" + this.state.editBookData.id, { title, rating }).then((response) => {
      console.log(response.data, "put call data");
      this.setState(this.editBookData);
      this._refreshTable();
      this.toggleEditBookModel();

    }
    ).catch(error => console.log(error));
  }

  deleteBookHandler(id){
    Axios.delete("http://localhost:3000/books/" + id).then((response) =>{
      console.log(response.data, "Delete button click data");
      this._refreshTable();

    }
    )}

  render() {

    let books = this.state.books.map((eachBook) => {
      return (
        <tr key={eachBook.id}>
          <td>{eachBook.id}</td>
          <td>{eachBook.title}</td>
          <td>{eachBook.rating}</td>
          <td>
            <Button color="success" size="sm" className="mr-2" onClick={this.editBookDataModalHandler.bind(this, eachBook.id, eachBook.title, eachBook.rating)}>Edit</Button>
            <Button color="danger" size="sm" className="mr-2" onClick={this.deleteBookHandler.bind(this, eachBook.id)}>Delete</Button>
          </td>
        </tr>
      )
    })

    return (
      <div className="App container">
        <Container className="themed-container" fluid={true}>

          <h2>Hi! Shubham's Book App Welcomes You :)</h2>
          <br></br>

          <div>
            <Button color="primary" onClick={this.toggleNewBookModel.bind(this)}>Add new book</Button>
            <Modal isOpen={this.state.newBookModal} toggle={this.toggleNewBookModel.bind(this)}>
              <ModalHeader toggle={this.toggleNewBookModel.bind(this)}>Add a new book</ModalHeader>
              <ModalBody>
                <Form>
                  <FormGroup>
                    <Label for="title">Title</Label>
                    <Input id="title" value={this.state.newBookData.title} onChange={
                      (event) => {
                        let { newBookData } = this.state; /* getting value from state property
                       "newBookData" as a variabe (called object destructuring) */
                        console.log("newBookData: ", newBookData);

                        newBookData.title = event.target.value;

                        console.log("event.target.value:", event.target.value); /* for this onchange event,
                       target is "input" element, and hence we have access to it's attributes like 
                       "id","type" and "value" */
                        console.log("event.target: ", event.target);

                        this.setState({ newBookData });
                      }
                    } />
                    <Label for="rating">Rating</Label>
                    <Input id="rating" value={this.state.newBookData.rating} onChange={
                      (e) => {
                        let { newBookData } = this.state; //getting the newBookData
                        newBookData.rating = e.target.value; //setting the newBookData rating
                        this.setState({ newBookData }); //passing it to state
                      }

                    } />
                  </FormGroup>
                </Form>
              </ModalBody>
              <ModalFooter>
                <Button type="submit" color="success" onClick={this.addBook.bind(this)}>Confirm Add </Button>
                <Button color="secondary" onClick={this.toggleNewBookModel.bind(this)}>I changed my mind</Button>
              </ModalFooter>
            </Modal>
          </div>
          <div>
            <Modal isOpen={this.state.editBookModal} toggle={this.toggleEditBookModel.bind(this)}>
              <ModalHeader toggle={this.toggleEditBookModel.bind(this)}>Edit a book</ModalHeader>
              <ModalBody>
                <Form>
                  <FormGroup>
                    <Label for="title">Title</Label>
                    <Input id="title" value={this.state.editBookData.title} onChange={
                      (event) => {
                        let { editBookData } = this.state; /* getting value from state property
                       "newBookData" as a variabe (called object destructuring) */
                        console.log("editBookData: ", editBookData);

                        editBookData.title = event.target.value;

                        console.log("event.target.value:", event.target.value); /* for this onchange event,
                       target is "input" element, and hence we have access to it's attributes like 
                       "id","type" and "value" */
                        console.log("event.target: ", event.target);

                        this.setState({ editBookData });
                      }
                    } />
                    <Label for="rating">Rating</Label>
                    <Input id="rating" value={this.state.editBookData.rating} onChange={
                      (e) => {
                        let { editBookData } = this.state; //getting the newBookData
                        editBookData.rating = e.target.value; //setting the newBookData rating
                        this.setState({ editBookData }); //passing it to state
                      }

                    } />
                  </FormGroup>
                </Form>
              </ModalBody>
              <ModalFooter>
                <Button type="submit" color="success" onClick={this.updateBookData.bind(this)}>Update Book</Button>
                <Button color="secondary" onClick={this.toggleEditBookModel.bind(this)}>I changed my mind</Button>
              </ModalFooter>
            </Modal>
          </div >

          <Table responsive>
            <thead>
              <tr>
                <th>Sr No</th>
                <th>Title</th>
                <th>Rating</th>
                <th>Actions</th>
              </tr>
            </thead>

            <tbody>
              {books}
            </tbody>
          </Table>
        </Container>
      </div >
    );
  }
}

export default App;